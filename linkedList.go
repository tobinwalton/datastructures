package datastructures

import "errors"

// Node represents an element in a LinkedList
type Node struct {
	next  *Node
	Value interface{}
}

// LinkedList represents a list of nodes or elements
type LinkedList struct {
	Head *Node
	Tail *Node
	Size int
}

// Append takes a value and adds a Node to the LinkedList
func (l *LinkedList) Append(value interface{}) {
	n := Node{nil, value}

	// Head is only nil when the list has no Nodes
	if l.Head == nil {
		l.Head = &n
		l.Tail = &n
	}

	// If there is a head, point old tail.next to new tail and reassign tail
	if l.Size >= 1 {
		l.Tail.next = &n
		l.Tail = &n
	}

	l.Size++
}

// Insert creates a new Node at the specified index, returns error if index is out of bounds
func (l *LinkedList) Insert(index int, value interface{}) error {
	var n = Node{nil, value}

	if index == l.Size-1 {
		l.Append(value)
		return nil
	}

	if index > 0 && index < l.Size {
		left, err := l.Get(index - 1)
		if err != nil {
			return err
		}
		right := left.next
		n.next = right
		left.next = &n
	} else if index == 0 {
		nextNode := l.Head
		n.next = nextNode
		l.Head = &n
	}

	l.Size++

	return nil
}

// Remove deletes a node at the specified index, returns error if index is out of bounds
func (l *LinkedList) Remove(index int) error {
	if index >= l.Size {
		err := errors.New("LinedList Remove: index out of bounds")
		return err
	}

	n, err := l.Get(index)
	if err != nil {
		return err
	}

	if l.Tail == n {
		newTail, err := l.Get(index - 1)
		if err != nil {
			return err
		}
		newTail.next = nil
		l.Tail = newTail
	} else if l.Head == n {
		newHead, err := l.Get(index + 1)
		if err != nil {
			return err
		}
		l.Head = newHead
	} else {
		left, err := l.Get(index - 1)
		if err != nil {
			return err
		}
		left.next = n.next
	}

	n.next = nil
	n.Value = nil
	l.Size--

	return nil
}

// Get returns a Node at a given index
func (l *LinkedList) Get(index int) (*Node, error) {
	node := l.Head

	if index >= l.Size {
		err := errors.New("LinkedList Get: index out of bounds")
		return nil, err
	}

	for i := 0; i < index; i++ {
		n := node.next
		node = n
	}

	return node, nil
}
